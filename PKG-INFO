Metadata-Version: 1.1
Name: flufl.password
Version: 1.3
Summary: Password hashing and verification.
Home-page: http://launchpad.net/flufl.password
Author: Barry Warsaw
Author-email: barry@python.org
License: LGPLv3
Download-URL: https://launchpad.net/flufl.password/+download
Description:    ==============
           flufl.password
           ==============
           
           Password hashing and verification.
           
           The ``flufl.password`` library provides hashing and verification of passwords.
           
           
           License
           =======
           
           This file is part of flufl.password.
           
           flufl.password is free software: you can redistribute it and/or modify it
           under the terms of the GNU Lesser General Public License as published by
           the Free Software Foundation, version 3 of the License.
           
           flufl.password is distributed in the hope that it will be useful, but
           WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
           or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
           License for more details.
           
           You should have received a copy of the GNU Lesser General Public License
           along with flufl.password.  If not, see <http://www.gnu.org/licenses/>.
        
           =======================
           NEWS for flufl.password
           =======================
           
           1.3 (2014-09-24)
           ================
            * Fix documentation bug.  (LP: #1026403)
            * Purge all references to `distribute`.
            * Describe the switch to git and the repository move.
           
           
           1.2.1 (2012-04-19)
           ==================
            * Add classifiers to setup.py and make the long description more compatible
              with the Cheeseshop.
            * Other changes to make the Cheeseshop page look nicer.  (LP: #680136)
            * setup_helper.py version 2.1.
           
           
           1.2 (2012-01-23)
           ================
            * Fix some packaging issues.
            * Remove tox.ini.
            * Bump Copyright years.
            * Update standard template.
            * Eliminate the need to use 2to3, and fix some Python 3 deprecations.
           
           
           1.1.1 (2012-01-01)
           ==================
            * Ensure all built-in schemes are registered by importing them in the
              __init__.py file.
           
           
           1.1 (2011-12-31)
           ================
            * Add user-friendly password generation API.
           
           
           1.0 (2011-12-31)
           ================
            * Initial release.
        
        
Platform: UNKNOWN
Classifier: Development Status :: 5 - Production/Stable
Classifier: Intended Audience :: Developers
Classifier: License :: OSI Approved :: GNU Lesser General Public License v3 or later (LGPLv3+)
Classifier: Operating System :: POSIX
Classifier: Operating System :: Microsoft :: Windows
Classifier: Operating System :: MacOS :: MacOS X
Classifier: Programming Language :: Python
Classifier: Programming Language :: Python :: 2.6
Classifier: Programming Language :: Python :: 2.7
Classifier: Programming Language :: Python :: 3
Classifier: Topic :: Software Development :: Libraries
Classifier: Topic :: Software Development :: Libraries :: Python Modules
